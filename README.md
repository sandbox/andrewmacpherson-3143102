Drush config-exclude commands
=============================

Drush commands to install (and uninstall) modules which are excluded from config synchronization.

Background
----------
Drupal 8.8.0 introduced the `$settings['config_exclude_modules']` feature, so that <a href="https://www.drupal.org/node/3079028">modules can be excluded from the configuration synchronization</a>. Typically, this is used to make sure that development utility modules don't get enabled on the production environment.

Drush Commands
--------------
This project provides some Drush commands which make use of the config-exclude feature:

* `drush config-exclude:install` - installs all of the modules listed in `$settings['config_exclude_modules']`.
* `drush config-exclude:uninstall` - un-installs all of the modules listed in `$settings['config_exclude_modules']`.

Use cases
----------
Many developers have a set of favourite modules they like to enable on their local development environments, and they enable them right away after importing a copy of the production database. Perhaps they even maintain a custom script to enable them all at once.

My idea is that `$settings['config_exclude_modules']` can serve an extra purpose. If we assume the list of modules excluded from config synchronization is _the same as the list of modules we want to use in development_, then a command which installs all the excluded modules will come in handy.

This assumption can work quite well for solo developers (or teams who can all agree on which development modules they like). On the other hand, this approach might not be so useful if a team has front-end and back-end developers who like a different set of modules.
